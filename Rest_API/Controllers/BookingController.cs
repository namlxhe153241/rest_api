﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Data;
using new_api.Models;

namespace Rest_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]

    public class BookingController : ControllerBase
    {
        traveltestContext context = new traveltestContext();
        DataTable table = new DataTable();
        private readonly IConfiguration _configuration;
        public BookingController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
       
        [HttpGet]
        public JsonResult Booking()
        {
            string query = "SELECT Booking.id,Booking.Email,Booking.name,Booking.phone,Booking.start_date,Booking.end_date,Booking.num_people,Booking.message,Vehicle.name,Hotel.name" +
                "\r\nFROM Booking\r\nINNER JOIN Vehicle ON Booking.vehicle_id = Vehicle.id\r\nINNER JOIN Hotel ON Booking.hotel_id = Hotel.id;\r\n\r\n\r\n";
            DataTable table = new DataTable();
            String sqlDatasource = _configuration.GetConnectionString("MyCnn");
            SqlDataReader myRedear;
            using (SqlConnection myCon = new SqlConnection(sqlDatasource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myRedear = myCommand.ExecuteReader();
                    table.Load(myRedear);
                    myRedear.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }

        [Route("GetAllBooking")]
        [HttpGet]
        public JsonResult GetBooking()
        {
            string query = "Select*  from Booking ";
            DataTable table = new DataTable();
            String sqlDatasource = _configuration.GetConnectionString("MyCnn");
            SqlDataReader myRedear;
            using(SqlConnection myCon = new SqlConnection(sqlDatasource))
            {
                myCon.Open();
                using(SqlCommand myCommand = new SqlCommand(query,myCon))
                {
                    myRedear = myCommand.ExecuteReader();
                    table.Load(myRedear);
                    myRedear.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }

        [Route("GetBookingByEmail")]
        [HttpGet]
        public JsonResult GetBookingByEmail(String Email)
        {
            
            string query = "SELECT Booking.id,Booking.Email,Booking.name,Booking.phone,Booking.start_date,Booking.end_date,Booking.num_people,Booking.message,Vehicle.name,Hotel.name" +
                "\r\nFROM Booking\r\nINNER JOIN Vehicle ON Booking.vehicle_id = Vehicle.id\r\nINNER JOIN Hotel ON Booking.hotel_id = Hotel.id\r\nwhere Booking.Email='"+Email+"';";
            DataTable table = new DataTable();
            String sqlDatasource = _configuration.GetConnectionString("MyCnn");
            SqlDataReader myRedear;
            using (SqlConnection myCon = new SqlConnection(sqlDatasource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myRedear = myCommand.ExecuteReader();
                    table.Load(myRedear);
                    myRedear.Close();
                    myCon.Close();
                }
            }
            return new JsonResult(table);
        }

    }
}
