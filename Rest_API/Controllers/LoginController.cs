﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Net.Mail;
using System.Net;

using new_api.Models;
using static System.Net.WebRequestMethods;
using TravelSystem_SWP391.DAO_Context;

namespace Rest_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        SendEmail send = new SendEmail();
        traveltestContext context = new traveltestContext();
        DataTable table = new DataTable();
        private readonly IConfiguration _configuration;
        public LoginController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpPost("SendMail")]
        public JsonResult SendEmail([FromBody] SampleDataModelS data)
        {
            String UserName = HttpContext.Session.GetString("UserName");
            string fromEmail = "namlxhe153241@fpt.edu.vn";
            string toEmail = UserName;
            string subject = "Hello " + UserName;
            string body =
                "Tạo Tài Khoản Thành Công";
            string smtpServer = "smtp.gmail.com";
            int smtpPort = 587;
            string smtpUsername = "namlxhe153241@fpt.edu.vn";
            string smtpPassword = "esot ywmu zsji tlqf";


            bool result = theSendEmail(fromEmail, toEmail, subject, body, smtpServer, smtpPort, smtpUsername, smtpPassword, data.username, data.pass, data.cf_Pass, data.firstName, data.lastName, data.phoneNumber);
            // .theSendEmail((data.fromEmail, data.toEmail, data.subject, data.body, data.smtpServer, data.smtpPort, data.smtpUsername, data.smtpPassword, data.username, data.pass, data.cf_Pass, data.firstName, data.lastName,
            //data.phoneNumber))
            return new JsonResult("Send Thanh Cong");
            


        }
        [HttpPost("Login")]
        public JsonResult Login([FromBody] SampleDataModelL data)
        {

           
            string smtpUsername = data.username;
            string smtpPassword = data.pass;
            
            if(context.Users.FirstOrDefault(s=>s.Email==data.username&&s.Password==data.pass)!=null){
                HttpContext.Session.SetString("UserName", data.username.ToString());
                HttpContext.Session.SetString("Pass", data.pass.ToString());
                return new JsonResult("Login Thành Công");
            }
            else
            {
                return new JsonResult("Login Không Thành  Công");
            }

           
           



        }




        [HttpPost("signin")]
        public async Task<ActionResult<User>> SignIn([FromBody] User user)
        {
            // Kiểm tra xác thực tài khoản
            var existingUser =  context.Users.FirstOrDefault(u => u.Email == user.Email && u.Password == user.Password);
            if (existingUser == null)
            {
                return Unauthorized();
            }

            // Trả về thông tin tài khoản
            return existingUser;
        }



        [HttpPost("GetSession")]
        public JsonResult GetSession()
        {


            String UserName = HttpContext.Session.GetString("UserName");
            String Pass = HttpContext.Session.GetString("Pass");
            if (UserName != null && Pass != null)
            {
                return new JsonResult(UserName+"  vs  "+Pass);
            }
            else
            {
                return new JsonResult("Session = null");
            }
            
            






        }
        public class SampleDataModelL
        {
            //sample data của model có 1 string: SampleData
            //SampleDataModel xử lý các Json có dạng {SampleData: ""}



        


            public string username { get; set; }
            public string pass { get; set; }
            

        }
        public class SampleDataModelS
        {
            //sample data của model có 1 string: SampleData
            //SampleDataModel xử lý các Json có dạng {SampleData: ""}
           
           
           
            public string toEmail { get; set; }

          
            public string username { get; set; }
            public string pass { get; set; }
            public string cf_Pass { get; set; }
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string phoneNumber { get; set; }

        }
        public static bool theSendEmail(string fromEmail, string toEmail, string subject, string body, string smtpServer, int smtpPort, string smtpUsername, string smtpPassword, string username, string pass, string cf_Pass, string firstName, string lastName,
          string phoneNumber)
        {

            DAO dal = new DAO();
            try
            {
                SmtpClient smtpClient = new SmtpClient(smtpServer);
                smtpClient.Port = smtpPort;
                smtpClient.Credentials = new NetworkCredential(smtpUsername, smtpPassword);
                smtpClient.EnableSsl = true; // Enable SSL for secure communication with the SMTP server

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(fromEmail);
                mailMessage.To.Add(toEmail);
                mailMessage.Subject = subject;
                if (pass == cf_Pass && dal.IsPhoneNumberValidVietnam(phoneNumber) == true && dal.IsValidFirstnameorLastname(firstName) == true && dal.IsValidFirstnameorLastname(lastName) == true)
                {
                    mailMessage.Body = body;
                    smtpClient.Send(mailMessage);
                    return true; // Email sent successfully
                }
                else
                {
                    mailMessage.Body = "Tạo Tài Khoản Không Thành Công Vui Lòng Kiểm Tra Lại Các Thông Tin!!!";
                }


                smtpClient.Send(mailMessage);
                return true; // Email sent successfully
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred: " + ex.Message);
                return false; // Email sending failed
            }
        }
    }
}
