﻿using Microsoft.AspNetCore.Mvc;
using System.Data;
using TravelSystem_SWP391.DAO_Context;
using new_api.Models;
using iText.Kernel.Pdf;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;


namespace Rest_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReadController : ControllerBase
    {
        SendEmail send = new SendEmail();
        traveltestContext context = new traveltestContext();
        DataTable table = new DataTable();
        private readonly IConfiguration _configuration;

        public ReadController(IConfiguration configuration)
        {
            _configuration = configuration;
        }




        [HttpPost("ReadFile")]
        public JsonResult ReadFile()
        {

            string pdfFilePath = "E:\\H2Q_Solution\\Rest_API\\Rest_API\\chuky.pdf";

            //ReadDigitalSignatures(pdfFilePath);

         
            return new JsonResult("Doc File "+pdfFilePath+" Khong Thanh Cong");
            
        }
        [HttpPost("CreateFile")]
        public JsonResult CreateFile(SampleDataModelText f)
        {

            string pdfFilePath = "E:\\H2Q_Solution\\Rest_API\\filepdf\\ghi.pdf";



           
            CreateAndWritePDF(pdfFilePath,f);
            return new JsonResult("Ghi File " + pdfFilePath + " Thanh Cong");

        }
        [HttpPost("CreateFileDoc")]
        public JsonResult CreateFileDoc(SampleDataModelText t )
        {

            CreateWordDocument("E:\\H2Q_Solution\\Rest_API\\Rest_API\\filepdf\\ghi.docx",t);




         
            return new JsonResult("Ghi File Thanh Cong");

        }


        private void CreateWordDocument(string filePath, SampleDataModelText t)
        {
            try
            {
                using (WordprocessingDocument wordDocument = WordprocessingDocument.Create(filePath, WordprocessingDocumentType.Document))
                {
                    // Add a main document part
                    MainDocumentPart mainPart = wordDocument.AddMainDocumentPart();

                    // Create the Document and Body elements
                    Document doc = new Document();
                    Body body = new Body();

                    // Add content to the document
                    Paragraph paragraph = new Paragraph();
                    Run run = new Run(new Text(t.Text));
                    paragraph.Append(run);
                    body.Append(paragraph);

                    // Append the Body to the Document
                    doc.Append(body);

                    // Append the Document to the MainDocumentPart
                    mainPart.Document = doc;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }



        }


        private void CreateAndWritePDF(string filePath, SampleDataModelText f)
        {
            try
            {
                using (var writer = new PdfWriter(filePath))
                {
                    using (var pdf = new PdfDocument(writer))
                    {
                        var document = new iText.Layout.Document(pdf);

                        // Thêm nội dung vào PDF
                       
                        //document.Add(new Paragraph(f.Text));

                        // Thêm các phần tử khác như hình ảnh, bảng, văn bản, vv.
                        // document.Add(new Image("path_to_image.jpg"));
                        // document.Add(new Table().AddCell("Cell 1").AddCell("Cell 2"));

                        // Có thể thêm nhiều phần tử khác vào tùy theo nhu cầu

                        // Kết thúc và đóng tài liệu để lưu các thay đổi
                        document.Close();
                    }
                }
            }catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
           
          
            
        }
        //public void ReadDigitalSignatures(string filePath)
        //{
        //    try
        //    {
        //        using (PdfReader pdfReader = new PdfReader(filePath))
        //        {
        //            using (PdfDocument pdfDocument = new PdfDocument(pdfReader))
        //            {
        //                SignatureUtil signatureUtil = new SignatureUtil(pdfDocument);

        //                // Get the list of signature names
        //                IList<string> signatureNames = signatureUtil.GetSignatureNames();

        //                foreach (string signatureName in signatureNames)
        //                {
        //                    // Extract signature information
        //                    PdfPKCS7 pdfPKCS7 = signatureUtil.ReadSignatureData(signatureName);
        //                    Org.BouncyCastle.X509.X509Certificate cert = (Org.BouncyCastle.X509.X509Certificate)pdfPKCS7.GetSigningCertificate();

        //                    // Extract signer's name
        //                    string signerName = GetSignerName(cert);

        //                    // Access signing date correctly
        //                    string signDate = pdfPKCS7.SignDate.ToString("yyyy-MM-dd HH:mm:ss");

        //                    // Display signature information
        //                    Console.WriteLine($"Signature Name: {signatureName}");
        //                    Console.WriteLine($"Signer Name: {signerName}");
        //                    Console.WriteLine($"Sign Date: {signDate}");
        //                    Console.WriteLine($"Covers Whole Document: {signatureUtil.GetSignatureCoversWholeDocument(signatureName)}");
        //                    Console.WriteLine();
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine($"Error reading digital signatures: {ex.Message}");
        //    }
        //}
        //public string GetSignerName(Org.BouncyCastle.X509.X509Certificate certificate)
        //{
        //    try
        //    {
        //        // Extract signer's name from the certificate
        //        return certificate.SubjectDN.GetValueList()[0]?.ToString();
        //    }
        //    catch
        //    {
        //        return "Unknown Signer";
        //    }
        //}


        [HttpPost("CopyFile")]
        public JsonResult CopyFile()
        {

            

            string sourceFilePath = "E:\\H2Q_Solution\\Rest_API\\Rest_API\\chuky.docx";
            string destinationFilePath = "E:\\H2Q_Solution\\Rest_API\\Rest_API\\filepdf\\ghi.docx";

            copyfile(sourceFilePath, destinationFilePath);

            return new JsonResult("Ghi File Thanh Cong");

        }

        private void copyfile(string sourceFilePath, string destinationFilePath)
        {
            try
            {
                // Mở tệp nguồn để đọc
                using (WordprocessingDocument sourceDocument = WordprocessingDocument.Open(sourceFilePath, false))
                {
                    // Mở tệp đích để ghi
                    using (WordprocessingDocument destinationDocument = WordprocessingDocument.Create(destinationFilePath, WordprocessingDocumentType.Document))
                    {
                        // Sao chép toàn bộ phần nội dung
                        destinationDocument.AddMainDocumentPart().AddPart(sourceDocument.MainDocumentPart);
                    }
                }

                Console.WriteLine("File copied successfully.");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error copying Word document: {ex.Message}");
            }


        }

        public class SampleDataModelFi
        {
            //sample data của model có 1 string: SampleData
            //SampleDataModel xử lý các Json có dạng {SampleData: ""}


            public string Name { get; set; }

            public string Type { get; set; }


        }
        public class SampleDataModelText
        {
            //sample data của model có 1 string: SampleData
            //SampleDataModel xử lý các Json có dạng {SampleData: ""}


            public string Text { get; set; }

           


        }

    }
}

