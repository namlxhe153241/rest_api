﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using new_api.Models;

namespace Rest_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        traveltestContext context = new traveltestContext();
        DataTable table = new DataTable();
        private readonly IConfiguration _configuration;
        public VehicleController(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        [HttpGet("GetVehicle")]
        public JsonResult GetVehicle() {
            var c = context.Vehicles.ToList();
            return new JsonResult(c);
        }
        [HttpPost("AddVehicle")]
        public JsonResult AddVehicle([FromBody] SampleDataModelF data)
        {
           


            if (data.Rate>0&&data.Rate <=5)
            {
               Vehicle  vehicle = new Vehicle()
                {

                    Name = data.Name,
                    Type = data.Type,
                    Price = data.Price,
                    Image = data.Image,
                    Rate = data.Rate,
                    Description = data.Description
                };
                context.Add(vehicle);
                context.SaveChanges();
                
                return new JsonResult("Add Thanh Cong");
            }
            else
            {
                return new JsonResult("Add That Bai");
            }
           
           
        }
        [HttpPut]
        public JsonResult EditVehicle([FromBody] SampleDataModelF1 data)
        {
            var vehicleedit=context.Vehicles.FirstOrDefault(v => v.Id== data.Id);

            if (vehicleedit==null)
            {
                return new JsonResult("Id Error"); 
            }
            if (data.Rate > 0 && data.Rate <= 5)
            {
                vehicleedit.Name= data.Name;
                vehicleedit.Type= data.Type;
                vehicleedit.Price=data.Price;
                vehicleedit.Image= data.Image;
                vehicleedit.Rate= data.Rate;
                vehicleedit.Description= data.Description;
                
                context.SaveChanges();

                return new JsonResult("Edit Thanh Cong");
            }
            else
            {
                return new JsonResult("Edit That Bai");
            }


        }
        [HttpDelete("DeleteVehicle/{id}")]
        public JsonResult DeleteVehicle(int id)
        {
            var vehicledelete = context.Vehicles.FirstOrDefault(v => v.Id == id);


            if (vehicledelete != null)
            {
                context.Remove(vehicledelete);
                context.SaveChanges();
                return new JsonResult("Delete Thanh Cong");
            }
            else
            {
                return new JsonResult("Delete That Bai ID Không Tồn Tại");
            }


        }
        public class SampleDataModelF
        {
            //sample data của model có 1 string: SampleData
            //SampleDataModel xử lý các Json có dạng {SampleData: ""}

           
            public string Name { get; set; }
            
            public string Type { get; set; }
            public Double Price { get; set; }
            public string Image { get; set; }
            public int Rate { get; set; }
            public string Description { get; set; }

        }
        public class SampleDataModelF1
        {
            //sample data của model có 1 string: SampleData
            //SampleDataModel xử lý các Json có dạng {SampleData: ""}

            public int Id { get; set; }
            public string Name { get; set; }

            public string Type { get; set; }
            public Double Price { get; set; }
            public string Image { get; set; }
            public int Rate { get; set; }
            public string Description { get; set; }

        }
    }
}
